from service.template_service import TemplateService
from utils.box_converter import convert_box_to_list
from service.UserService import UserService
from pymongo import MongoClient
from flask import Flask, request, Response, json, send_file, after_this_request
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename
from pathlib import Path
from utils.utils import Job_Status, Output_File, Document_Status
from datetime import datetime, timezone
from io import FileIO
from PyPDF2 import PdfFileWriter, PdfFileReader
from service.front_page_service import FrontPageHandler
from threading import Thread

import configparser
import uuid
import os
import redis
import json
import pandas as pd
import pytz
import socketio
import shutil
import time

import pyrebase

firebaseConfig = {
    "apiKey": "AIzaSyAPHNsT4BQjbvC_NNgu0BB3YXPZy1vioNU",
    "authDomain": "projet4-bcfe5.firebaseapp.com",
    "projectId": "projet4-bcfe5",
    "storageBucket": "projet4-bcfe5.appspot.com",
    "messagingSenderId": "171342986362",
    "appId": "1:171342986362:web:018b05e620c609a2ce0fc3",
    "measurementId": "G-E9P71RH1DF",
    "databaseURL": "",
    "serviceAccount": str(Path(__file__).parent.joinpath("config").joinpath("projet4_service_account.json"))
}

TEMP_FOLDER = Path(__file__).resolve().parent.joinpath("temp")

VALIDATE_TEMP_FOLDER = Path(__file__).resolve().parent.joinpath("validate_temp_folder")

CONFIG_FILE = Path(__file__).resolve().parent.joinpath("config").joinpath("config.ini")

FRONT_PAGE_TEMP_FOLDER = Path(__file__).resolve().parent.joinpath("front_page_temp")

LATEX_INPUT_FILE = Path(__file__).resolve().parent.joinpath("data.tex")

app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"

parser = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
parser.read(CONFIG_FILE)

URL = parser.get("MONGODB", "URL")

mongo_client = MongoClient(URL)

redis_host = "redis" if os.getenv("ENVIRONNEMENT") == "production" else "localhost"
socketio_host = (
    "socketio" if os.getenv("ENVIRONNEMENT") == "production" else "localhost"
)

r = redis.Redis(host=redis_host, port=6379, db=0)


@app.route("/")
def say_hello():
    return "<h1>Hi Hello Andy</h1>"


@app.route("/login", methods=["POST"])
@cross_origin()
def login():
    db = mongo_client["RMN"]
    return UserService.login(request, db)


@app.route("/signup", methods=["POST"])
@cross_origin()
def signup():
    db = mongo_client["RMN"]
    return UserService.signup(request, db)


@app.route("/updateSaveVerifiedImages", methods=["PUT"])
@cross_origin()
def update_user():
    db = mongo_client["RMN"]
    return UserService.update_save_verified_images(request, db)

@app.route("/updateMoodleStructureInd", methods=["PUT"])
@cross_origin()
def update_moodle_structure_ind():
    db = mongo_client["RMN"]
    return UserService.update_moodle_structure_ind(request, db)


@app.route("/password", methods=["POST"])
@cross_origin()
def change_password():
    db = mongo_client["RMN"]
    return UserService.change_password(request, db)


@app.route("/evaluate", methods=["POST"])
@cross_origin()
def evaluate():
    request_form = request.form

    if "user_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: user_id not provided."}),
            status=400,
        )

    if "template_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: template_id not provided."}),
            status=400,
        )

    if "nb_pages" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: nb_pages not provided."}),
            status=400,
        )

    if "job_name" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_name not provided."}),
            status=400,
        )

    if "template_name" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: template_name not provided."}),
            status=400,
        )

    print(request.files)

    if not request.files:
        return Response(
            response=json.dumps({"response": f"Error: No files provided."}),
            status=400,
        )

    if "notes_csv_file" not in request.files:
        return Response(
            response=json.dumps({"response": f"Error: Notes csv file not provided."}),
            status=400,
        )

    if "zip_file" not in request.files:
        return Response(
            response=json.dumps({"response": f"Error: Zip file not provided."}),
            status=400,
        )

    user_id = str(request_form["user_id"])

    template_id = str(request_form["template_id"])
    template_name = str(request_form["template_name"])
    job_name = str(request_form["job_name"])
    nb_pages = int(request_form["nb_pages"])

    # Define db and collection used
    db = mongo_client["RMN"]
    collection = db["eval_jobs"]

    job_id = str(uuid.uuid4())

    path_on_cloud_zip = "zips/"
    zip_file_id = f"{path_on_cloud_zip}{job_id}.zip"

    path_on_cloud_csv = "csv/"
    notes_file_id = f"{path_on_cloud_csv}{job_id}.csv"
    
    job = {
        "job_id": job_id,
        "job_name": job_name,
        "user_id": user_id,
        "template_id": template_id,
        "template_name": template_name,
        "queued_time": datetime.utcnow(),
        "job_status": Job_Status.QUEUED.value,
        "notes_file_id": notes_file_id,
        "zip_file_id": zip_file_id,
    }

    try:
        collection.insert_one(job)
    except Exception as e:
        print(e)
        return Response(
            response=json.dumps({"response": f"Error: Failed to insert in MongoDB."}),
            status=500,
        )

    if not os.path.exists(TEMP_FOLDER):
        os.makedirs(TEMP_FOLDER)
    try:
        notes_csv_file = request.files.get("notes_csv_file")
        notes_csv_file_name = secure_filename(notes_csv_file.filename)
        notes_csv_file.save(FileIO(TEMP_FOLDER.joinpath(notes_csv_file_name), "wb"))

        zip_file = request.files.get("zip_file")
        zip_file_name = secure_filename(zip_file.filename)

        is_pdf_file = str(zip_file_name).endswith(".pdf")
        # transform into zip file
        if is_pdf_file:
            input_pdf = PdfFileReader(zip_file)

            folder_to_zip = TEMP_FOLDER.joinpath("folder_to_zip")

            if not os.path.exists(folder_to_zip):
                os.makedirs(folder_to_zip)

            current_idx = 0
            output = PdfFileWriter()
            for i in range(input_pdf.numPages):
                output.addPage(input_pdf.getPage(i))

                current_idx += 1

                if current_idx == nb_pages:
                    current_idx = 0
                    with open(
                        str(folder_to_zip.joinpath(f"{str(i)}.pdf")), "wb"
                    ) as out:
                        output.write(out)
                    output = PdfFileWriter()

            zip_file_name = "copies.zip"
            shutil.make_archive(
                str(TEMP_FOLDER.joinpath("copies")), "zip", str(folder_to_zip)
            )

            shutil.rmtree(str(folder_to_zip))
        else:
            with open(str(TEMP_FOLDER.joinpath(zip_file_name)), "wb") as f_out:
                print("here")
                file_content = zip_file.stream.read()
                f_out.write(file_content)

    except Exception as e:
        print(e)
        return Response(response=f"Error: Failed to download files.", status=900)

    # Create SocketIO connection
    sio = socketio.Client()
    print(socketio_host)
    sio.connect(f"http://{socketio_host}:7000")

    sio.emit(
        "jobs_status",
        json.dumps(
            {"job_id": job_id, "status": Job_Status.QUEUED.value, "user_id": user_id}
        ),
    )

    sio.disconnect()

    thread = Thread(target=evaluate_thread, 
    kwargs={
        "job_id": job_id,
        "notes_file_id": notes_file_id,
        "zip_file_id": zip_file_id, 
        "job": job, 
        "user_id": user_id, 
        "zip_file_name": zip_file_name, 
        "notes_csv_file_name": notes_csv_file_name,
        "is_pdf_file": is_pdf_file

    })
    thread.start()

    return Response(response=json.dumps({"response": "OK"}), status=200)

def evaluate_thread(job_id, notes_file_id, zip_file_id, job, user_id, zip_file_name, notes_csv_file_name, is_pdf_file):
    try:
        file_name = str(TEMP_FOLDER.joinpath(zip_file_name))
        firebase_storage = pyrebase.initialize_app(firebaseConfig).storage()

        firebase_storage.child(zip_file_id).put(file_name)

        file_name = str(TEMP_FOLDER.joinpath(notes_csv_file_name))
        firebase_storage.child(notes_file_id).put(file_name)
    except Exception as e:
        print(e, "311")

        # Create SocketIO connection
        sio = socketio.Client()
        print(socketio_host)
        sio.connect(f"http://{socketio_host}:7000")

        db = mongo_client["RMN"]
        collection_eval_jobs = db["eval_jobs"]

        collection_eval_jobs.update_one(
                {"job_id": job_id},
                {
                    "$set": {
                        "job_status": Job_Status.ERROR.value,
                    }
                },
            )

        sio.emit(
            "jobs_status",
            json.dumps(
                {
                    "job_id": job_id,
                    "user_id": user_id,
                    "status": Job_Status.ERROR.value,
                }
            ),
        )
        sio.disconnect()
        exit()

    # delete temp files
    os.remove(str(TEMP_FOLDER.joinpath(notes_csv_file_name)))
    os.remove(str(TEMP_FOLDER.joinpath(zip_file_name)))

    # add to Redis Queue
    queue_object = {
        "job_type": "execution",
        "job_id": job["job_id"],
        "user_id": job["user_id"],
        "is_pdf_file": is_pdf_file
    }
    r.rpush("job_queue", json.dumps(queue_object))


@app.route("/template", methods=["POST"])
@cross_origin()
def create_template():
    db = mongo_client["RMN"]
    return TemplateService.create_template(request, db, firebaseConfig)


@app.route("/user/template", methods=["POST"])
@cross_origin()
def get_all_template_info():
    db = mongo_client["RMN"]
    return TemplateService.get_all_template_info(request, db)


@app.route("/template/delete", methods=["POST"])
@cross_origin()
def delete_template():
    db = mongo_client["RMN"]
    return TemplateService.delete_template(request, db, firebaseConfig)


@app.route("/template/info", methods=["POST"])
@cross_origin()
def get_template_info():
    db = mongo_client["RMN"]
    return TemplateService.get_template_info(request, db)


@app.route("/template/download", methods=["POST"])
@cross_origin()
def download_template():
    db = mongo_client["RMN"]
    return TemplateService.download_template_file(request, db, firebaseConfig)


@app.route("/template/modify", methods=["POST"])
@cross_origin()
def modify_template():
    db = mongo_client["RMN"]
    return TemplateService.change_template_info(request, db)


@app.route("/jobs", methods=["POST"])
@cross_origin()
def get_jobs():
    # Define db and collection used
    db = mongo_client["RMN"]
    collection = db["eval_jobs"]
    template_collection = db["template"]

    request_form = request.form
    user_id = str(request_form["user_id"])

    if "user_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: user_id not provided."}),
            status=400,
        )

    # Get all jobs from DB
    jobs = collection.find({"user_id": user_id})

    #
    resp = [
        {
            "job_id": job["job_id"],
            "template_id": job["template_id"],
            "queued_time": str(job["queued_time"]),
            "job_status": job["job_status"],
            "job_name": job["job_name"],
            "template_name": job["template_name"],
        }
        for job in jobs
    ]

    #
    return Response(response=json.dumps({"response": resp}), status=200)


@app.route("/file/download", methods=["POST"])
@cross_origin()
def download_file():
    #
    request_form = request.form

    #
    if "job_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_id not provided."}),
            status=400,
        )

    #
    if "file" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: file not provided."}),
            status=400,
        )

    #
    job_id = str(request_form["job_id"])

    # TODO: validation pour job_id

    target_file = str(request_form["file"])
    try:
        target_file = Output_File(target_file)
    except Exception as e:
        print(e)
        return Response(
            response=json.dumps(
                {"response": f"Error: invalid value for 'file' param."}
            ),
            status=400,
        )
    if "zip_index" not in request_form and target_file == Output_File.ZIP_FILE:
        return Response(
            response=json.dumps({"response": f"Error: zip_index not provided."}),
            status=400,
        )

    #
    db = mongo_client["RMN"]
    output_collection = db["jobs_output"]

    firebase_storage = pyrebase.initialize_app(firebaseConfig).storage()

    #
    output_files = output_collection.find_one({"job_id": job_id})

    #
    output_file_mapping_dict = {
        Output_File.NOTES_CSV_FILE: "notes_csv_file_id",
        Output_File.PREVIEW_FILE: "preview_file_id",
        Output_File.ZIP_FILE: "moodle_zip_id_list",
    }

    target_output_file = output_file_mapping_dict[target_file]

    file_id = output_files[target_output_file]

    if target_file == Output_File.ZIP_FILE:
        zip_index = int(request_form["zip_index"])
        print(output_files)
        print(output_files[target_output_file])
        file_id = output_files[target_output_file][zip_index]

    if not os.path.exists(TEMP_FOLDER):
        os.makedirs(TEMP_FOLDER)

    # Save file to local
    filepath = str(TEMP_FOLDER.joinpath(file_id.split("/")[1]))
    firebase_storage.child(file_id).download(filepath)
    print("file created")

    file_send = send_file(filepath)

    os.remove(filepath)

    return file_send


@app.route("/job/batch/info", methods=["POST"])
@cross_origin()
def get_info_zip():
    request_form = request.form

    if "job_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_id not provided."}),
            status=400,
        )
    #
    job_id = str(request_form["job_id"])

    #
    db = mongo_client["RMN"]
    output_collection = db["jobs_output"]

    #
    output_files = output_collection.find_one({"job_id": job_id})
    print(output_files)
    #
    resp = len(output_files["moodle_zip_id_list"])

    #
    return Response(response=json.dumps({"response": resp}), status=200)


@app.route("/documents", methods=["POST"])
@cross_origin()
def get_documents():
    request_form = request.form

    if "job_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_id not provided."}),
            status=400,
        )

    #
    job_id = str(request_form["job_id"])

    #
    db = mongo_client["RMN"]
    collection = db["job_documents"]

    #
    docs = collection.find({"job_id": job_id})
    count = collection.count_documents({"job_id": job_id})

    #
    resp = [
        {
            "job_id": doc["job_id"],
            "document_index": doc["document_index"],
            "subquestion_predictions": doc["subquestion_predictions"],
            "matricule": doc["matricule"],
            "total": doc["total"],
            "status": doc["status"],
            "students_list": doc["students_list"],
            "exec_time": doc["execution_time"],
            "n_total_doc": count,
        }
        for doc in docs
    ]

    #
    return Response(response=json.dumps({"response": resp}), status=200)


@app.route("/documents/update", methods=["POST"])
@cross_origin()
def update_document():
    request_form = request.form

    if "job_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_id not provided."}),
            status=400,
        )

    if "document_index" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: document_index not provided."}),
            status=400,
        )

    if "matricule" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: matricule not provided."}),
            status=400,
        )

    if "subquestion_predictions" not in request_form:
        return Response(
            response=json.dumps(
                {"response": f"Error: subquestion_predictions not provided."}
            ),
            status=400,
        )

    if "total" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: total not provided."}),
            status=400,
        )

    if "status" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: document_index not provided."}),
            status=400,
        )

    print(request_form)

    #
    job_id = str(request_form["job_id"])
    document_index = int(request_form["document_index"])
    matricule = str(request_form["matricule"])
    subquestion_predictions = json.loads(request_form["subquestion_predictions"])
    total = float(request_form["total"])
    status = Document_Status(str(request_form["status"]))

    #
    db = mongo_client["RMN"]
    collection = db["job_documents"]

    #
    collection.update_one(
        {"job_id": job_id, "document_index": document_index},
        {
            "$set": {
                "matricule": matricule,
                "subquestion_predictions": subquestion_predictions,
                "total": total,
                "status": Document_Status.VALIDATED.value,
            }
        },
    )

    return Response(response=json.dumps({"response": "OK"}), status=200)


@app.route("/document/download", methods=["POST"])
@cross_origin()
def download_document():
    #
    request_form = request.form

    if "job_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_id not provided."}),
            status=400,
        )

    if "document_index" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: document_index not provided."}),
            status=400,
        )

    #
    job_id = str(request_form["job_id"])
    document_index = int(request_form["document_index"])

    #
    db = mongo_client["RMN"]
    document_collection = db["job_documents"]

    #
    document_file = document_collection.find_one(
        {"job_id": job_id, "document_index": document_index}
    )

    #
    file_id = document_file["image_id"]
    if document_file is None:
        return Response(
            response=json.dumps({"response": f"No document found!"}),
            status=404,
        )

    firebase_storage = pyrebase.initialize_app(firebaseConfig).storage()

    if not os.path.exists("documents"):
            os.mkdir("documents")

    open(str(file_id), "w+")

    # Save file to local
    print("file_id", file_id)
    firebase_storage.child(file_id).download(str(file_id))
    print("file created")

    file_send = send_file(str(file_id))

    time.sleep(0.1)

    @after_this_request
    def add_close_action(response):
        try:
            os.remove(str(file_id))
        except Exception as e:
            print(e)
        return response

    return file_send


@app.route("/job/validate", methods=["POST"])
@cross_origin()
def validate():
    #
    request_form = request.form

    #
    if "job_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_id not provided."}),
            status=400,
        )
    if "user_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: user_id not provided."}),
            status=400,
        )

    if "moodle_ind" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: moodle_ind not provided."}),
            status=400,
        )

    #
    job_id = str(request_form["job_id"])
    user_id = str(request_form["user_id"])
    moodle_ind = request_form["moodle_ind"]

    db = mongo_client["RMN"]
    collection = db["job_documents"]
    collection_eval_jobs = db["eval_jobs"]

    # Set Job status to VALIDATION
    collection_eval_jobs.update_one(
        {"job_id": job_id}, {"$set": {"job_status": Job_Status.VALIDATING.value}}
    )

    # Create SocketIO connection
    sio = socketio.Client()
    print(socketio_host)
    sio.connect(f"http://{socketio_host}:7000")

    sio.emit(
        "jobs_status",
        json.dumps(
            {
                "job_id": job_id,
                "status": Job_Status.VALIDATING.value,
                "user_id": user_id,
            }
        ),
    )

    # add to Redis Queue
    queue_object = {
        "job_type": "validation",
        "job_id": job_id,
        "user_id": user_id,
        "moodle_ind": moodle_ind,
    }
    try:
        r.rpush("job_queue", json.dumps(queue_object))
    except Exception as e:
        print(e)
        print("Failed to push job to Redis Queue.")
        return Response(
            response=json.dumps(
                {"response": f"Error: Failed to push job to Redis Queue."}
            ),
            status=500,
        )

    # set all estimation to 0
    collection.update_many(
        {"job_id": job_id},
        {
            "$set": {
                "execution_time": 0,
            }
        },
    )

    return Response(response=json.dumps({"response": "OK"}), status=200)


@app.route("/job/delete", methods=["POST"])
@cross_origin()
def delete():
    request_form = request.form

    if "job_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: job_id not provided."}),
            status=400,
        )

    #
    job_id = str(request_form["job_id"])

    #
    db = mongo_client["RMN"]
    collection_eval_jobs = db["eval_jobs"]
    collection_output = db["jobs_output"]
    firebase_storage = pyrebase.initialize_app(firebaseConfig).storage()

    try:
        for f in [
            (
                "output_csv/",
                ".csv",
            ),
            ("output_zip/", ".zip"),
        ]:
            fullpath = f"{f[0]}{job_id}{f[1]}"
            firebase_storage.delete(fullpath)
    except Exception as e:
        print(e)

    #
    try:
        collection_eval_jobs.delete_many({"job_id": job_id})
    except Exception as e:
        print(e)

    try:
        collection_output.delete_many({"job_id": job_id})
    except Exception as e:
        print(e)

    #
    return Response(response=json.dumps({"response": "OK"}), status=200)


@app.route("/front_page", methods=["POST"])
@cross_origin()
def front_page():
    request_form = request.form

    if "user_id" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: user_id not provided."}),
            status=400,
        )

    if "suffix" not in request_form:
        return Response(
            response=json.dumps({"response": f"Error: suffix not provided."}),
            status=400,
        )

    if not request.files:
        return Response(
            response=json.dumps({"response": f"Error: No files provided."}),
            status=400,
        )

    if "moodle_zip" not in request.files:
        return Response(
            response=json.dumps({"response": f"Error: moodle_zip file not provided."}),
            status=400,
        )

    if "latex_front_page" not in request.files:
        return Response(
            response=json.dumps(
                {"response": f"Error: latex_front_page file not provided."}
            ),
            status=400,
        )


    user_id = str(request_form["user_id"])
    suffix = str(request_form["suffix"])
    moodle_zip = request.files.get("moodle_zip")
    latex_front_page = request.files.get("latex_front_page")

    print(user_id)
    print(suffix)

    if not os.path.exists(FRONT_PAGE_TEMP_FOLDER):
        os.makedirs(FRONT_PAGE_TEMP_FOLDER)

    current_temp_folder = FRONT_PAGE_TEMP_FOLDER.joinpath(user_id)

    if not os.path.exists(current_temp_folder):
        os.makedirs(current_temp_folder)

    moodle_zip_name = secure_filename(moodle_zip.filename)
    latex_front_page_name = secure_filename(latex_front_page.filename)

    # Copy latex_input_file in temp_folder
    shutil.copy(LATEX_INPUT_FILE, current_temp_folder)
    
    moodle_zip_filepath = str(current_temp_folder.joinpath(moodle_zip_name))
    latex_front_page_filepath = str(current_temp_folder.joinpath(latex_front_page_name))
    latex_input_file_filepath = str(current_temp_folder.joinpath("data.tex"))

    with open(moodle_zip_filepath, "wb") as f_out:
        file_content = moodle_zip.stream.read()
        f_out.write(file_content)

    content_temp_folder = current_temp_folder.joinpath("moodle")
    if not os.path.exists(content_temp_folder):
        os.makedirs(content_temp_folder)

    shutil.unpack_archive(moodle_zip_filepath, content_temp_folder)
    os.remove(moodle_zip_filepath)

    latex_front_page.save(FileIO(latex_front_page_filepath, "wb"))

    handler = FrontPageHandler()
    handler.addFrontPages(
        str(current_temp_folder),
        content_temp_folder,
        suffix,
        latex_front_page_filepath,
        latex_input_file_filepath,
    )

    shutil.make_archive(str(content_temp_folder), "zip", content_temp_folder)

    file_send = send_file(f"{str(content_temp_folder)}.zip")

    shutil.rmtree(str(current_temp_folder))

    return file_send


if __name__ == "__main__":
    app.run(debug=True)
